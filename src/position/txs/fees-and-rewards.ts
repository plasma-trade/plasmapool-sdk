import { deriveATA, resolveOrCreateATA } from "@plasmatrade-so/common-sdk";
import { MultiTransactionBuilder } from "../../utils/public/multi-transaction-builder";
import {
  CollectFeesAndRewardsTxParam,
  CollectMultipleFeesAndRewardsTxParam,
} from "../public/types";
import { PlasmaTradeDAL } from "../../dal/plasmatrade-dal";
import {
  EMPTY_INSTRUCTION,
  Instruction,
  NUM_REWARDS,
  PositionData,
  TransactionBuilder,
  PlasmaPoolClient,
  PlasmaPoolContext,
} from "@plasmatrade-so/plasmapool-client-sdk";
import { TickUtil } from "../../utils/plasmapool/tick-util";
import { toPubKey } from "../../utils/address";
import { PublicKey } from "@solana/web3.js";
import invariant from "tiny-invariant";
import { PoolUtil } from "../../utils/plasmapool/pool-util";
import { Address } from "@project-serum/anchor/dist/cjs/program/common";
import { Provider } from "@project-serum/anchor";
import { NATIVE_MINT } from "@solana/spl-token";

export async function buildMultipleCollectFeesAndRewardsTx(
  dal: PlasmaTradeDAL,
  param: CollectMultipleFeesAndRewardsTxParam
): Promise<MultiTransactionBuilder> {
  const { provider, positionAddresses, resolvedAssociatedTokenAddresses } = param;

  const ctx = PlasmaPoolContext.withProvider(provider, dal.programId);
  const client = new PlasmaPoolClient(ctx);

  const collectPositionTransactions: TransactionBuilder[] = [];

  // If we don't create an empty map here when resolvedAssociatedTokenAddresses is undefined,
  // then the ataMap ends up getting set in the buildSingleCollectFeeAndRewardsTx
  // and not shared across the multiple fee collection txs
  const ataMap = resolvedAssociatedTokenAddresses ?? {};
  for (const positionAddress of positionAddresses) {
    const txn = await buildSingleCollectFeeAndRewardsTx(
      positionAddress,
      dal,
      client,
      provider,
      ataMap
    );

    if (!txn.isEmpty()) {
      collectPositionTransactions.push(txn);
    }
  }

  /**
   * TODO: Find the maximum number of collect position calls we can fit in a transaction.
   * Note that the calls may not be the same size. The maximum size is a collect ix where
   * 1. TokenMintA requires a create ATA ix
   * 2. TokenMintB is a SOL account. Requires the create & clean up WSOL ATA ix
   * 3. Position liquidity is not null. updateFee Ix is required
   * 4. Need to collect fees
   * 5. Need to collect all 3 rewards
   *  */

  const collectAllTransactionBuilder = new MultiTransactionBuilder(provider, []);

  collectPositionTransactions.forEach((collectTxn) =>
    collectAllTransactionBuilder.addTxBuilder(collectTxn)
  );

  return collectAllTransactionBuilder;
}

export async function buildCollectFeesAndRewardsTx(
  dal: PlasmaTradeDAL,
  param: CollectFeesAndRewardsTxParam
): Promise<TransactionBuilder> {
  const { provider, positionAddress, resolvedAssociatedTokenAddresses } = param;

  const ctx = PlasmaPoolContext.withProvider(provider, dal.programId);
  const client = new PlasmaPoolClient(ctx);

  return await buildSingleCollectFeeAndRewardsTx(
    positionAddress,
    dal,
    client,
    provider,
    resolvedAssociatedTokenAddresses
  );
}

async function buildSingleCollectFeeAndRewardsTx(
  positionAddress: Address,
  dal: PlasmaTradeDAL,
  client: PlasmaPoolClient,
  provider: Provider,
  ataMap?: Record<string, PublicKey>
): Promise<TransactionBuilder> {
  const txn: TransactionBuilder = new TransactionBuilder(provider);
  const positionInfo = await derivePositionInfo(positionAddress, dal, provider.wallet.publicKey);
  if (positionInfo == null) {
    return txn;
  }

  const {
    position,
    plasmapool,
    tickArrayLower,
    tickArrayUpper,
    positionTokenAccount,
    nothingToCollect,
  } = positionInfo;

  if (nothingToCollect) {
    return txn;
  }

  if (!ataMap) {
    ataMap = {};
  }

  // Derive and add the createATA instructions for each token mint. Note that
  // if the user already has the token ATAs, the instructions will be empty.
  const {
    tokenOwnerAccount: tokenOwnerAccountA,
    createTokenOwnerAccountIx: createTokenOwnerAccountAIx,
  } = await getTokenAtaAndPopulateATAMap(dal, provider, plasmapool.tokenMintA, ataMap);
  const {
    tokenOwnerAccount: tokenOwnerAccountB,
    createTokenOwnerAccountIx: createTokenOwnerAccountBIx,
  } = await getTokenAtaAndPopulateATAMap(dal, provider, plasmapool.tokenMintB, ataMap);
  txn.addInstruction(createTokenOwnerAccountAIx).addInstruction(createTokenOwnerAccountBIx);

  // If the position has zero liquidity, then the fees are already the most up to date.
  // No need to make an update call here.
  if (!position.liquidity.isZero()) {
    txn.addInstruction(
      client
        .updateFeesAndRewards({
          plasmapool: position.plasmapool,
          position: toPubKey(positionAddress),
          tickArrayLower,
          tickArrayUpper,
        })
        .compressIx(false)
    );
  }

  // Add a collectFee ix for this position
  txn.addInstruction(
    client
      .collectFeesTx({
        plasmapool: position.plasmapool,
        positionAuthority: provider.wallet.publicKey,
        position: toPubKey(positionAddress),
        positionTokenAccount,
        tokenOwnerAccountA,
        tokenOwnerAccountB,
        tokenVaultA: plasmapool.tokenVaultA,
        tokenVaultB: plasmapool.tokenVaultB,
      })
      .compressIx(false)
  );

  // Add a collectReward ix for a reward mint if the particular reward is initialized.
  for (const i of [...Array(NUM_REWARDS).keys()]) {
    const rewardInfo = plasmapool.rewardInfos[i];
    invariant(!!rewardInfo, "rewardInfo cannot be undefined");

    if (!PoolUtil.isRewardInitialized(rewardInfo)) {
      continue;
    }

    const {
      tokenOwnerAccount: rewardOwnerAccount,
      createTokenOwnerAccountIx: createRewardTokenOwnerAccountIx,
    } = await getTokenAtaAndPopulateATAMap(dal, provider, rewardInfo.mint, ataMap);

    if (createRewardTokenOwnerAccountIx) {
      txn.addInstruction(createRewardTokenOwnerAccountIx);
    }

    txn.addInstruction(
      client
        .collectRewardTx({
          plasmapool: position.plasmapool,
          positionAuthority: provider.wallet.publicKey,
          position: toPubKey(positionAddress),
          positionTokenAccount,
          rewardOwnerAccount,
          rewardVault: rewardInfo.vault,
          rewardIndex: i,
        })
        .compressIx(false)
    );
  }

  return txn;
}

async function getTokenAtaAndPopulateATAMap(
  dal: PlasmaTradeDAL,
  provider: Provider,
  tokenMint: PublicKey,
  ataMap: Record<string, PublicKey>
) {
  let _tokenMintA = tokenMint.toBase58();

  let tokenOwnerAccount: PublicKey;
  let createTokenOwnerAccountIx: Instruction = EMPTY_INSTRUCTION;
  const mappedTokenAAddress = ataMap[_tokenMintA];

  if (!mappedTokenAAddress) {
    const { address: _tokenOwnerAccount, ..._tokenOwnerAccountAIx } = await resolveOrCreateATA(
      provider.connection,
      provider.wallet.publicKey,
      tokenMint,
      () => dal.getAccountRentExempt()
    );
    tokenOwnerAccount = _tokenOwnerAccount;
    createTokenOwnerAccountIx = _tokenOwnerAccountAIx;

    if (!tokenMint.equals(NATIVE_MINT)) {
      ataMap[_tokenMintA] = _tokenOwnerAccount;
    }
  } else {
    tokenOwnerAccount = mappedTokenAAddress;
  }

  return { tokenOwnerAccount, createTokenOwnerAccountIx };
}

async function derivePositionInfo(positionAddress: Address, dal: PlasmaTradeDAL, walletKey: PublicKey) {
  const position = await dal.getPosition(positionAddress, false);
  if (!position) {
    return null;
  }

  const plasmapool = await dal.getPool(position.plasmapool, false);
  if (!plasmapool) {
    return null;
  }

  const [tickArrayLower, tickArrayUpper] = TickUtil.getLowerAndUpperTickArrayAddresses(
    position.tickLowerIndex,
    position.tickUpperIndex,
    plasmapool.tickSpacing,
    position.plasmapool,
    dal.programId
  );

  const positionTokenAccount = await deriveATA(walletKey, position.positionMint);

  const nothingToCollect =
    position.liquidity.isZero() && !hasOwedFees(position) && !hasOwedRewards(position);
  return {
    position,
    plasmapool,
    tickArrayLower,
    tickArrayUpper,
    positionTokenAccount,
    nothingToCollect,
  };
}

function hasOwedFees(position: PositionData) {
  return !(position.feeOwedA.isZero() && position.feeOwedB.isZero());
}

function hasOwedRewards(position: PositionData) {
  return position.rewardInfos.some((rewardInfo) => !rewardInfo.amountOwed.isZero());
}
