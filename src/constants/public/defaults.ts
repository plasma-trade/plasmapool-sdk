import { Connection } from "@solana/web3.js";
import { Percentage } from "../../utils/public/percentage";
import { PlasmaTradeNetwork } from "./network";

export const defaultSlippagePercentage = Percentage.fromFraction(1, 1000); // 0.1%
export const ZERO_SLIPPAGE = Percentage.fromFraction(0, 1000);

export const defaultNetwork: PlasmaTradeNetwork = PlasmaTradeNetwork.MAINNET;

export function getDefaultConnection(network: PlasmaTradeNetwork): Connection {
  switch (network) {
    case PlasmaTradeNetwork.MAINNET:
      return new Connection("https://ssc-dao.genesysgo.net", "processed");
    case PlasmaTradeNetwork.DEVNET:
      return new Connection("https://api.devnet.solana.com", "processed");
    default:
      throw new Error(`type ${network} is an Unknown network`);
  }
}

export function getDefaultOffchainDataURI(network: PlasmaTradeNetwork): string {
  switch (network) {
    case PlasmaTradeNetwork.MAINNET:
      return "https://api.mainnet.plasmatrade.so/v1";
    case PlasmaTradeNetwork.DEVNET:
      return "https://api.devnet.plasmatrade.so/v1";
    default:
      throw new Error(`type ${network} is an Unknown network`);
  }
}
