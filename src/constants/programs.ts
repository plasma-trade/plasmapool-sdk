import { PublicKey } from "@solana/web3.js";
import { PlasmaTradeNetwork } from "./public/network";

export function getPlasmaPoolsConfig(network: PlasmaTradeNetwork): PublicKey {
  switch (network) {
    case PlasmaTradeNetwork.MAINNET:
      return new PublicKey("2LecshUwdy9xi7meFgHtFJQNSKk4KdTrcpvaB56dP2NQ");
    case PlasmaTradeNetwork.DEVNET:
      return new PublicKey("847gd7SckNwJetbjf22ktip9vAtKWPMY4ntdr6CdCsJj");
    default:
      throw new Error(`type ${network} is an Unknown network`);
  }
}

export function getPlasmaPoolProgramId(network: PlasmaTradeNetwork): PublicKey {
  switch (network) {
    case PlasmaTradeNetwork.MAINNET:
      return new PublicKey("whirLbMiicVdio4qvUfM5KAg6Ct8VwpYzGff3uctyCc");
    case PlasmaTradeNetwork.DEVNET:
      return new PublicKey("whirLbMiicVdio4qvUfM5KAg6Ct8VwpYzGff3uctyCc");
    default:
      throw new Error(`type ${network} is an Unknown network`);
  }
}
