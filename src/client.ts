import { Address } from "@project-serum/anchor";
import { NATIVE_MINT } from "@solana/spl-token";
import { Connection } from "@solana/web3.js";
import Decimal from "decimal.js";
import { PlasmaTradeNetwork } from "./constants/public/network";
import { PoolData } from "./types";
import { PlasmaTradeAdmin } from "./admin/plasmatrade-admin";
import {
  defaultNetwork,
  getDefaultConnection,
  getDefaultOffchainDataURI,
} from "./constants/public/defaults";
import { getPlasmaPoolProgramId, getPlasmaPoolsConfig } from "./constants/programs";
import { PlasmaTradeDAL } from "./dal/plasmatrade-dal";
import { PlasmaTradePool } from "./pool/plasmatrade-pool";
import { PlasmaTradePosition } from "./position/plasmatrade-position";
import { getTokenUSDPrices, TokenUSDPrices } from "./utils/token-price";
import { convertPlasmaPoolDataToPoolData } from "./pool/convert-data";
import { UserPositionData } from "./types";
import { convertPositionDataToUserPositionData } from "./position/convert-data";
import { PlasmaPoolData } from "@plasmatrade-so/plasmapool-client-sdk";
import { PlasmaTradeZooplankton } from "./offchain/plasmatrade-zp";
import { toPubKey } from "./utils/address";

// Global rules for Decimals
//  - 40 digits of precision for the largest number
//  - 20 digits of precision for the smallest number
//  - Always round towards 0 to mirror smart contract rules
Decimal.set({ precision: 40, toExpPos: 40, toExpNeg: -20, rounding: 1 });

export type PlasmaTradePlasmaPoolClientConfig = {
  network?: PlasmaTradeNetwork;
  connection?: Connection;
  plasmapoolConfig?: Address;
  programId?: Address;
  offchainDataURI?: string;
};

export class PlasmaTradePlasmaPoolClient {
  public readonly data: PlasmaTradeDAL;
  public readonly admin: PlasmaTradeAdmin;
  public readonly pool: PlasmaTradePool;
  public readonly position: PlasmaTradePosition;
  public readonly offchain: PlasmaTradeZooplankton;

  constructor(config?: PlasmaTradePlasmaPoolClientConfig) {
    const network = config?.network || defaultNetwork;
    const connection = config?.connection || getDefaultConnection(network);
    const plasmapoolsConfig = config?.plasmapoolConfig || getPlasmaPoolsConfig(network);
    const programId = config?.programId || getPlasmaPoolProgramId(network);
    const offchainDataURI = config?.offchainDataURI || getDefaultOffchainDataURI(network);

    this.data = new PlasmaTradeDAL(plasmapoolsConfig, programId, connection);
    this.admin = new PlasmaTradeAdmin(this.data);
    this.pool = new PlasmaTradePool(this.data);
    this.position = new PlasmaTradePosition(this.data);
    this.offchain = new PlasmaTradeZooplankton(offchainDataURI);
  }

  /**
   * Use on-chain dex data to derive usd prices for tokens.
   *
   * @param poolAddresses pools to be used for price discovery
   * @param baseTokenMint a token mint with known stable usd price (e.g. USDC)
   * @param baseTokenUSDPrice baseTokenMint's usd price. defaults to 1, assuming `baseTokenMint` is a USD stable coin
   * @param otherBaseTokenMints optional list of token mints to prioritize as base
   * @param refresh defaults to refreshing the cache
   */
  public async getTokenPrices(
    poolAddresses: Address[],
    baseTokenMint: Address,
    baseTokenUSDPrice = new Decimal(1),
    otherBaseTokenMints: Address[] = [NATIVE_MINT],
    refresh = true
  ): Promise<TokenUSDPrices> {
    const allPools = await this.data.listPools(poolAddresses, refresh);
    const pools = allPools.filter((pool): pool is PlasmaPoolData => pool !== null);
    return getTokenUSDPrices(
      this.data,
      pools,
      baseTokenMint,
      baseTokenUSDPrice,
      otherBaseTokenMints
    );
  }

  /**
   * Fetch position data owned by the wallet address.
   *
   * @param walletAddress wallet address
   * @param refresh defaults to refreshing the cache
   * @returns positions owned by the wallet address
   */
  public async getUserPositions(
    walletAddress: Address,
    refresh = true
  ): Promise<Record<string, UserPositionData>> {
    return convertPositionDataToUserPositionData(this.data, walletAddress, refresh);
  }

  /**
   * Fetch list of pool data.
   *
   * @param poolAddresses list of pools to retrieve
   * @param refresh defaults to refreshing the cache
   * @returns list of pool data
   */
  public async getPools(
    poolAddresses: Address[],
    refresh = true
  ): Promise<Record<string, PoolData>> {
    return convertPlasmaPoolDataToPoolData(this.data, poolAddresses, refresh);
  }

  /**
   * Fetch pool data.
   *
   * @param poolAddress pool address
   * @param refresh defaults to refreshing the cache
   * @returns pool data
   */
  public async getPool(poolAddress: Address, refresh = true): Promise<PoolData | null> {
    const pool = (await this.getPools([poolAddress], refresh))[toPubKey(poolAddress).toBase58()];
    return pool || null;
  }
}
