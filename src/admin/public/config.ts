import {
  TransactionBuilder,
  PlasmaPoolContext,
  PlasmaPoolClient,
  InitFeeTierParams,
  getFeeTierPda,
} from "@plasmatrade-so/plasmapool-client-sdk";
import { Address, Provider } from "@project-serum/anchor";
import { Keypair } from "@solana/web3.js";
import { toPubKey } from "../../utils/address";

export type InitPlasmaPoolConfigsTxParam = {
  programId: Address;
  provider: Provider;
  plasmapoolConfigKeypair: Keypair;
  feeAuthority: Address;
  collectProtocolFeesAuthority: Address;
  rewardEmissionsSuperAuthority: Address;
  defaultProtocolFeeRate: number;
};

export function getInitPlasmaPoolConfigsTx({
  programId,
  provider,
  plasmapoolConfigKeypair,
  feeAuthority,
  collectProtocolFeesAuthority,
  rewardEmissionsSuperAuthority,
  defaultProtocolFeeRate,
}: InitPlasmaPoolConfigsTxParam): TransactionBuilder {
  const ctx = PlasmaPoolContext.withProvider(provider, toPubKey(programId));
  const client = new PlasmaPoolClient(ctx);

  return client.initConfigTx({
    plasmapoolConfigKeypair,
    feeAuthority: toPubKey(feeAuthority),
    collectProtocolFeesAuthority: toPubKey(collectProtocolFeesAuthority),
    rewardEmissionsSuperAuthority: toPubKey(rewardEmissionsSuperAuthority),
    defaultProtocolFeeRate,
    funder: provider.wallet.publicKey,
  });
}

export type InitFeeTierConfigTxParam = {
  programId: Address;
  provider: Provider;
  plasmapoolConfigKey: Address;
  feeAuthority: Address;
  tickSpacing: number;
  defaultFeeRate: number;
};

export function getInitFeeTierConfigTx({
  programId,
  provider,
  plasmapoolConfigKey,
  feeAuthority,
  tickSpacing,
  defaultFeeRate,
}: InitFeeTierConfigTxParam): TransactionBuilder {
  const ctx = PlasmaPoolContext.withProvider(provider, toPubKey(programId));
  const client = new PlasmaPoolClient(ctx);
  const feeTierPda = getFeeTierPda(toPubKey(programId), toPubKey(plasmapoolConfigKey), tickSpacing);
  const params: InitFeeTierParams = {
    plasmapoolConfigKey: toPubKey(plasmapoolConfigKey),
    feeAuthority: toPubKey(feeAuthority),
    feeTierPda,
    tickSpacing,
    defaultFeeRate,
    funder: provider.wallet.publicKey,
  };
  return client.initFeeTierTx(params);
}
