import { resolveOrCreateATAs } from "@plasmatrade-so/common-sdk";
import { Keypair, PublicKey } from "@solana/web3.js";
import invariant from "tiny-invariant";
import {
  InitPoolTxParam,
  CollectProtocolFeesTxParam,
  SetFeeAuthorityTxParam,
  SetCollectProtocolFeesAuthorityTxParam,
  InitRewardTxParam,
  SetRewardAuthorityTxParam,
  SetRewardEmissionsTxParam,
  SetRewardAuthorityBySuperAuthorityTxParam,
  SetRewardEmissionsBySuperAuthorityTxParam,
  SetFeeRateTxParam,
  SetProtocolFeeRateTxParam,
  SetRewardSuperAuthorityTxParam,
} from "./public/types";
import { PlasmaTradeDAL } from "../dal/plasmatrade-dal";
import { toPubKey } from "../utils/address";
import {
  TransactionBuilder,
  PlasmaPoolContext,
  PlasmaPoolClient,
  getPlasmaPoolPda,
  NUM_REWARDS,
  getFeeTierPda,
} from "@plasmatrade-so/plasmapool-client-sdk";

export class PlasmaTradeAdmin {
  constructor(private readonly dal: PlasmaTradeDAL) {}

  public getInitPoolTx(param: InitPoolTxParam): { tx: TransactionBuilder; address: PublicKey } {
    const { provider, initSqrtPrice, tokenMintA, tokenMintB, tickSpacing } = param;
    const { programId, plasmapoolsConfig: plasmapoolConfigKey } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    const plasmapoolPda = getPlasmaPoolPda(
      programId,
      plasmapoolConfigKey,
      toPubKey(tokenMintA),
      toPubKey(tokenMintB),
      tickSpacing
    );

    const feeTierPda = getFeeTierPda(programId, plasmapoolConfigKey, tickSpacing);

    const tx = client.initPoolTx({
      initSqrtPrice,
      plasmapoolConfigKey,
      tokenMintA: toPubKey(tokenMintA),
      tokenMintB: toPubKey(tokenMintB),
      plasmapoolPda,
      tokenVaultAKeypair: Keypair.generate(),
      tokenVaultBKeypair: Keypair.generate(),
      tickSpacing,
      feeTierKey: feeTierPda.publicKey,
      funder: provider.wallet.publicKey,
    });

    return { tx, address: plasmapoolPda.publicKey };
  }

  /*** Fee ***/

  public async getCollectProtocolFeesTx(
    param: CollectProtocolFeesTxParam
  ): Promise<TransactionBuilder> {
    const { provider, poolAddress } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    const plasmapool = await this.dal.getPool(poolAddress, true);
    invariant(!!plasmapool, "PlasmaTradeAdmin - plasmapool does not exist");

    const [ataA, ataB] = await resolveOrCreateATAs(
      provider.connection,
      provider.wallet.publicKey,
      [{ tokenMint: plasmapool.tokenMintA }, { tokenMint: plasmapool.tokenMintB }],
      () => this.dal.getAccountRentExempt()
    );
    const { address: tokenDestinationA, ...createTokenAAtaIx } = ataA!;
    const { address: tokenDestinationB, ...createTokenBAtaIx } = ataB!;

    const collectFeesIx = client
      .collectProtocolFeesTx({
        plasmapoolsConfig,
        plasmapool: toPubKey(poolAddress),
        collectProtocolFeesAuthority: provider.wallet.publicKey,
        tokenVaultA: plasmapool.tokenVaultA,
        tokenVaultB: plasmapool.tokenVaultB,
        tokenDestinationA: toPubKey(tokenDestinationA),
        tokenDestinationB: toPubKey(tokenDestinationB),
      })
      .compressIx(false);

    return new TransactionBuilder(provider)
      .addInstruction(createTokenAAtaIx)
      .addInstruction(createTokenBAtaIx)
      .addInstruction(collectFeesIx);
  }

  public getSetFeeAuthorityTx(param: SetFeeAuthorityTxParam): TransactionBuilder {
    const { provider, newFeeAuthority } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    return client.setFeeAuthorityTx({
      plasmapoolsConfig,
      feeAuthority: provider.wallet.publicKey,
      newFeeAuthority: toPubKey(newFeeAuthority),
    });
  }

  public async getSetFeeRateTx(param: SetFeeRateTxParam): Promise<TransactionBuilder> {
    const { provider, feeRate, poolAddress } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    const plasmapoolsConfigAccount = await this.dal.getConfig(plasmapoolsConfig, true);
    invariant(
      !!plasmapoolsConfigAccount,
      `PlasmaTradeAdmin - PlasmaPool config doesn't exist ${plasmapoolsConfig.toBase58()}`
    );

    return client.setFeeRateIx({
      plasmapool: toPubKey(poolAddress),
      plasmapoolsConfig,
      feeAuthority: plasmapoolsConfigAccount.feeAuthority,
      feeRate,
    });
  }

  public async getSetProtocolFeeRateTx(
    param: SetProtocolFeeRateTxParam
  ): Promise<TransactionBuilder> {
    const { provider, protocolFeeRate, poolAddress } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    const plasmapoolsConfigAccount = await this.dal.getConfig(plasmapoolsConfig, true);
    invariant(
      !!plasmapoolsConfigAccount,
      `PlasmaTradeAdmin - PlasmaPool config doesn't exist ${plasmapoolsConfig.toBase58()}`
    );

    return client.setProtocolFeeRateIx({
      plasmapool: toPubKey(poolAddress),
      plasmapoolsConfig,
      feeAuthority: plasmapoolsConfigAccount.feeAuthority,
      protocolFeeRate,
    });
  }

  public getSetCollectProtocolFeesAuthorityTx(
    param: SetCollectProtocolFeesAuthorityTxParam
  ): TransactionBuilder {
    const { provider, newCollectProtocolFeesAuthority } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    return client.setCollectProtocolFeesAuthorityTx({
      plasmapoolsConfig,
      collectProtocolFeesAuthority: provider.wallet.publicKey,
      newCollectProtocolFeesAuthority: toPubKey(newCollectProtocolFeesAuthority),
    });
  }

  /*** Reward ***/

  public getInitRewardTx(param: InitRewardTxParam): {
    tx: TransactionBuilder;
    rewardVault: PublicKey;
  } {
    const { provider, rewardAuthority, poolAddress, rewardMint, rewardIndex } = param;
    const ctx = PlasmaPoolContext.withProvider(provider, this.dal.programId);
    const client = new PlasmaPoolClient(ctx);

    invariant(rewardIndex < NUM_REWARDS, "invalid rewardIndex");

    const rewardVaultKeypair = Keypair.generate();
    const tx = client.initializeRewardTx({
      rewardAuthority: toPubKey(rewardAuthority),
      funder: provider.wallet.publicKey,
      plasmapool: toPubKey(poolAddress),
      rewardMint: toPubKey(rewardMint),
      rewardVaultKeypair,
      rewardIndex,
    });

    return { tx, rewardVault: rewardVaultKeypair.publicKey };
  }

  public getSetRewardAuthorityTx(param: SetRewardAuthorityTxParam): TransactionBuilder {
    const { provider, poolAddress, newRewardAuthority, rewardIndex } = param;
    const ctx = PlasmaPoolContext.withProvider(provider, this.dal.programId);
    const client = new PlasmaPoolClient(ctx);

    invariant(rewardIndex < NUM_REWARDS, "invalid rewardIndex");

    return client.setRewardAuthorityTx({
      plasmapool: toPubKey(poolAddress),
      rewardAuthority: provider.wallet.publicKey,
      newRewardAuthority: toPubKey(newRewardAuthority),
      rewardIndex,
    });
  }

  public async getSetRewardEmissionsTx(
    param: SetRewardEmissionsTxParam
  ): Promise<TransactionBuilder> {
    const { provider, poolAddress, rewardIndex, emissionsPerSecondX64 } = param;
    const ctx = PlasmaPoolContext.withProvider(provider, this.dal.programId);
    const client = new PlasmaPoolClient(ctx);

    invariant(rewardIndex < NUM_REWARDS, "invalid rewardIndex");

    const plasmapool = await this.dal.getPool(poolAddress, true);
    const rewardVault = plasmapool?.rewardInfos[rewardIndex]?.vault;

    invariant(!!rewardVault, "reward vault doeos not exist");

    return client.setRewardEmissionsTx({
      rewardAuthority: provider.wallet.publicKey,
      plasmapool: toPubKey(poolAddress),
      rewardIndex,
      emissionsPerSecondX64,
      rewardVault,
    });
  }

  public getSetRewardAuthorityBySuperAuthorityTx(
    param: SetRewardAuthorityBySuperAuthorityTxParam
  ): TransactionBuilder {
    const { provider, poolAddress, newRewardAuthority, rewardIndex } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    invariant(rewardIndex < NUM_REWARDS, "invalid rewardIndex");

    return client.setRewardAuthorityBySuperAuthorityTx({
      plasmapoolsConfig,
      plasmapool: toPubKey(poolAddress),
      rewardEmissionsSuperAuthority: provider.wallet.publicKey,
      newRewardAuthority: toPubKey(newRewardAuthority),
      rewardIndex,
    });
  }

  public getSetRewardSuperAuthorityTx(param: SetRewardSuperAuthorityTxParam): TransactionBuilder {
    const { provider, newRewardSuperAuthority } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    return client.setRewardEmissionsSuperAuthorityTx({
      plasmapoolsConfig,
      rewardEmissionsSuperAuthority: provider.wallet.publicKey,
      newRewardEmissionsSuperAuthority: toPubKey(newRewardSuperAuthority),
    });
  }

  public getSetRewardEmissionsBySuperAuthorityTx(
    param: SetRewardEmissionsBySuperAuthorityTxParam
  ): TransactionBuilder {
    const { provider, rewardEmissionsSuperAuthority, newRewardEmissionsSuperAuthority } = param;
    const { programId, plasmapoolsConfig } = this.dal;
    const ctx = PlasmaPoolContext.withProvider(provider, programId);
    const client = new PlasmaPoolClient(ctx);

    return client.setRewardEmissionsSuperAuthorityTx({
      plasmapoolsConfig,
      rewardEmissionsSuperAuthority: toPubKey(rewardEmissionsSuperAuthority),
      newRewardEmissionsSuperAuthority: toPubKey(newRewardEmissionsSuperAuthority),
    });
  }
}
