import { Address } from "@project-serum/anchor";
import { PlasmaTradeDAL } from "../../dal/plasmatrade-dal";

export type PriceRange = {};

export type SuggestedPriceRanges = {
  conservative: PriceRange;
  standard: PriceRange;
};

export async function getSuggestedPriceRanges(
  dal: PlasmaTradeDAL,
  poolAddress: Address,
  refresh: boolean
): Promise<SuggestedPriceRanges> {
  throw new Error();
}
