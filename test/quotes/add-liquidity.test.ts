import {
  getPoolMock,
  getPositionMock,
  listMintInfosMock,
  PlasmaTradeDALFileMock,
} from "../mocks/plasmatrade-dal";
import { Connection, PublicKey } from "@solana/web3.js";
import BN from "bn.js";
import Decimal from "decimal.js";
import { u64 } from "@solana/spl-token";
import { PlasmaTradeDAL } from "../../src/dal/plasmatrade-dal";
import { PlasmaTradePosition } from "../../src/position/plasmatrade-position";
import { AddLiquidityQuote, AddLiquidityQuoteParam } from "../../src";
import { defaultSlippagePercentage } from "../../src/constants/public/defaults";
import {
  PositionData,
  PositionRewardInfoData,
  TickArrayData,
  PlasmaPoolData,
} from "@plasmatrade-so/plasmapool-client-sdk";
import { ZERO } from "../../src/utils/web3/math-utils";
const PlasmaPoolsJSON = require("./fixtures/add-liquidity/PlasmaPools.json");
const TickArraysJSON = require("./fixtures/add-liquidity/TickArrays.json");
const PositionsJSON = require("./fixtures/add-liquidity/Positions.json");

Decimal.set({ precision: 40, toExpPos: 40, toExpNeg: -20, rounding: 1 });

function deserializePlasmaPool(plasmapoolJson: Record<string, any>): PlasmaPoolData {
  return {
    plasmapoolsConfig: new PublicKey(plasmapoolJson.plasmapoolsConfig),
    plasmapoolBump: plasmapoolJson.plasmapoolBump,
    feeRate: plasmapoolJson.feeRate,
    protocolFeeRate: plasmapoolJson.protocolFeeRate,
    liquidity: new BN(plasmapoolJson.liquidity),
    sqrtPrice: new BN(plasmapoolJson.sqrtPrice),
    tickCurrentIndex: plasmapoolJson.tickCurrentIndex,
    protocolFeeOwedA: new BN(plasmapoolJson.protocolFeeOwedA),
    protocolFeeOwedB: new BN(plasmapoolJson.protocolFeeOwedB),
    tokenMintA: new PublicKey(plasmapoolJson.tokenMintA),
    tokenVaultA: new PublicKey(plasmapoolJson.tokenVaultA),
    feeGrowthGlobalA: new BN(plasmapoolJson.feeGrowthGlobalA),
    tokenMintB: new PublicKey(plasmapoolJson.tokenMintB),
    tokenVaultB: new PublicKey(plasmapoolJson.tokenVaultB),
    feeGrowthGlobalB: new BN(plasmapoolJson.feeGrowthGlobalA),
    rewardLastUpdatedTimestamp: new BN(plasmapoolJson.rewardLastUpdatedTimestamp),
    rewardInfos: plasmapoolJson.rewardInfos.map((infoJson: Record<string, any>) => ({
      mint: new PublicKey(infoJson.mint),
      vault: new PublicKey(infoJson.vault),
      authority: new PublicKey(infoJson.authority),
      emissionsPerSecondX64: new BN(infoJson.emissionsPerSecondX64),
      growthGlobalX64: new BN(infoJson.growthGlobalX64),
    })),
    tickSpacing: plasmapoolJson.tickSpacing,
  };
}

function deserializeTickArray(tickArrayJson: Record<string, any>): TickArrayData {
  return {
    plasmapool: new PublicKey(tickArrayJson.plasmapool),
    startTickIndex: tickArrayJson.startTickIndex,
    ticks: tickArrayJson.ticks.map((tickJson: Record<string, any>) => ({
      initialized: tickJson.initialized,
      liquidityNet: new BN(tickJson.liquidityNet),
      liquidityGross: new BN(tickJson.liquidityGross),
      feeGrowthOutsideA: new BN(tickJson.feeGrowthOutsideA),
      feeGrowthOutsideB: new BN(tickJson.feeGrowthOutsideB),
      rewardGrowthsOutside: tickJson.rewardGrowthsOutside.map(
        (rewardGrowth: string) => new BN(rewardGrowth)
      ),
    })),
  };
}

function deserializePosition(positionJson: Record<string, any>): PositionData {
  return {
    plasmapool: new PublicKey(positionJson.plasmapool),
    positionMint: new PublicKey(positionJson.positionMint),
    liquidity: new BN(positionJson.liquidity),
    tickLowerIndex: parseInt(positionJson.tickLowerIndex, 10),
    tickUpperIndex: parseInt(positionJson.tickUpperIndex, 10),
    feeGrowthCheckpointA: new BN(positionJson.feeGrowthCheckpointA),
    feeOwedA: new BN(positionJson.feeOwedA),
    feeGrowthCheckpointB: new BN(positionJson.feeGrowthCheckpointB),
    feeOwedB: new BN(positionJson.feeOwedB),
    rewardInfos: positionJson.rewardInfos.map(
      (info: Record<string, any>) =>
        ({
          growthInsideCheckpoint: new BN(info.growthInsideCheckpoint),
          amountOwed: new BN(info.amountOwed),
        } as PositionRewardInfoData)
    ),
  };
}

function serializeAddLiquidityQuote(addLiquidityQuote: AddLiquidityQuote): string {
  return JSON.stringify(
    {
      maxTokenA: addLiquidityQuote.maxTokenA.toString(),
      maxTokenB: addLiquidityQuote.maxTokenB.toString(),
      liquidity: addLiquidityQuote.liquidity.toString(),
    },
    null,
    2
  );
}

describe("Add Liquidity", () => {
  const plasmapoolsMap: Record<string, PlasmaPoolData> = Object.keys(PlasmaPoolsJSON).reduce(
    (map, key) => ({
      ...map,
      [key]: deserializePlasmaPool(PlasmaPoolsJSON[key]),
    }),
    {}
  );

  const tickArraysMap: Record<string, TickArrayData> = Object.keys(TickArraysJSON).reduce(
    (map, key) => ({
      ...map,
      [key]: deserializeTickArray(TickArraysJSON[key]),
    }),
    {}
  );

  const positionsMap: Record<string, PositionData> = Object.keys(PositionsJSON).reduce(
    (map, key) => ({
      ...map,
      [key]: deserializePosition(PositionsJSON[key]),
    }),
    {}
  );

  getPoolMock.mockImplementation((address: PublicKey) => plasmapoolsMap[address.toBase58()]);
  getPositionMock.mockImplementation((address: PublicKey) => positionsMap[address.toBase58()]);
  listMintInfosMock.mockImplementation((addresses: PublicKey[]) =>
    addresses.map(() => ({
      mintAuthority: null,
      supply: new u64(1e6),
      decimals: 0,
      isInitialized: true,
      freezeAuthority: null,
    }))
  );

  beforeEach(() => {
    PlasmaTradeDALFileMock.clearAllMocks();
  });

  test("base case: increase liquidity of a position spanning two tick arrays", async () => {
    const plasmapoolProgramId = new PublicKey("Fg6PaFpoGXkYsidMpWTK6W2BeZ7FEfcYkg476zPFsLnS");
    const plasmapoolAddress = new PublicKey("6wADQSNfubas7sExoKhoFo4vXM72RaYqin3mk7ce3tf7");
    const plasmapool = plasmapoolsMap[plasmapoolAddress.toBase58()];
    const tokenBAmount = new BN("16700");

    const expectedTokenAAmount = ZERO;
    const expectedLiquidityAmount = new BN("130386");
    const expectedTokenBAmount = new BN("16716");

    const mockDal = new PlasmaTradeDAL(
      PublicKey.default,
      PublicKey.default,
      new Connection("http://google.com")
    );

    const plasmatradePosition = new PlasmaTradePosition(mockDal);

    const params: AddLiquidityQuoteParam = {
      positionAddress: new PublicKey("5GhgBXfuKuFvqhz7h8LAQabEievmWgiXiiVwYHQFejLw"),
      tokenMint: plasmapool.tokenMintB,
      tokenAmount: tokenBAmount,
      refresh: true,
      slippageTolerance: defaultSlippagePercentage,
    };

    const addLiquidityQuote = await plasmatradePosition.getAddLiquidityQuote(params);

    expect(addLiquidityQuote.maxTokenA.toString()).toEqual(expectedTokenAAmount.toString());
    expect(addLiquidityQuote.maxTokenB.toString()).toEqual(expectedTokenBAmount.toString());
    expect(addLiquidityQuote.liquidity.toString()).toEqual(expectedLiquidityAmount.toString());
  });
});
