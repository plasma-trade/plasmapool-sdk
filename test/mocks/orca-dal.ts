import { PlasmaPoolData, PositionData, TickArrayData } from "@plasmatrade-so/plasmapool-client-sdk";
import { MintInfo } from "@solana/spl-token";
import { PublicKey } from "@solana/web3.js";

export const getPoolMock = jest.fn(
  (address: PublicKey, refresh: boolean = false) => ({} as PlasmaPoolData)
);
export const getPositionMock = jest.fn(
  (address: PublicKey, refresh: boolean = false) => ({} as PositionData)
);
export const getTickArrayMock = jest.fn(
  (address: PublicKey, refresh: boolean = false) => ({} as TickArrayData)
);
export const listMintInfosMock = jest.fn(
  (addresses: PublicKey[], refresh: boolean = false) => ({} as MintInfo[])
);
export const PlasmaTradeDALMock = jest.fn(function () {
  return {
    getPool: getPoolMock,
    getPosition: getPositionMock,
    getTickArray: getTickArrayMock,
    listMintInfos: listMintInfosMock,
  };
});

export const PlasmaTradeDALFileMock = jest.mock("../../src/dal/plasmatrade-dal", function () {
  return {
    PlasmaTradeDAL: PlasmaTradeDALMock,
  };
});
