import { toX64 } from "@plasmatrade-so/plasmapool-client-sdk";
import { BN, Provider } from "@project-serum/anchor";
import { u64 } from "@solana/spl-token";
import { Keypair, PublicKey } from "@solana/web3.js";
import Decimal from "decimal.js";
import {
  getInitFeeTierConfigTx,
  getInitPlasmaPoolConfigsTx,
  PlasmaTradePlasmaPoolClient,
  Percentage,
} from "../../src";
import { PlasmaTradeAdmin } from "../../src/admin/plasmatrade-admin";
import { ZERO } from "../../src/utils/web3/math-utils";
import { createAndMintToTokenAccount, createInOrderMints } from "./token";

export const DEFAULT_FEE_RATE = 3000;
export const DEFAULT_PROTOCOL_FEE_RATE = 300;
export const zeroSlippage = new Percentage(ZERO, new BN(100));

export async function initPlasmaPoolsConfig(
  provider: Provider,
  programId: PublicKey,
  owner: PublicKey,
  initTickSpacing = 64,
  initDefaultFee = DEFAULT_FEE_RATE
) {
  const plasmapoolsConfigKeypair = Keypair.generate();
  const plasmapoolsConfig = plasmapoolsConfigKeypair.publicKey;

  await getInitPlasmaPoolConfigsTx({
    programId,
    provider,
    plasmapoolConfigKeypair: plasmapoolsConfigKeypair,
    feeAuthority: owner,
    collectProtocolFeesAuthority: owner,
    rewardEmissionsSuperAuthority: owner,
    defaultProtocolFeeRate: DEFAULT_PROTOCOL_FEE_RATE,
  }).buildAndExecute();

  await getInitFeeTierConfigTx({
    programId,
    provider,
    plasmapoolConfigKey: plasmapoolsConfigKeypair.publicKey,
    feeAuthority: owner,
    tickSpacing: initTickSpacing,
    defaultFeeRate: initDefaultFee,
  }).buildAndExecute();

  return plasmapoolsConfig;
}

export async function initPool(
  plasmatradeAdmin: PlasmaTradeAdmin,
  provider: Provider,
  initSqrtPrice: BN,
  tickSpacing: number
) {
  const [tokenMintA, tokenMintB] = await createInOrderMints(provider);
  await createAndMintToTokenAccount(provider, tokenMintA, new u64("1000000000"));
  await createAndMintToTokenAccount(provider, tokenMintB, new u64("1000000000"));

  const { tx, address } = plasmatradeAdmin.getInitPoolTx({
    provider,
    initSqrtPrice,
    tokenMintA,
    tokenMintB,
    tickSpacing,
  });

  await tx.buildAndExecute();
  return {
    poolAddress: address,
    tokenMintA,
    tokenMintB,
  };
}

export type PoolLiquidityParam = {
  tickLowerIndex: number;
  tickUpperIndex: number;
  isTokenMintA: true;
  tokenAmount: BN;
};

export async function initStandardPoolWithLiquidity(
  client: PlasmaTradePlasmaPoolClient,
  plasmatradeAdmin: PlasmaTradeAdmin,
  provider: Provider
) {
  return initPoolWithLiquidity(client, plasmatradeAdmin, provider, toX64(new Decimal(1.0005)), 64, [
    {
      tickLowerIndex: -128,
      tickUpperIndex: 128,
      isTokenMintA: true,
      tokenAmount: new BN("100000000"),
    },
  ]);
}

export async function initPoolWithLiquidity(
  client: PlasmaTradePlasmaPoolClient,
  plasmatradeAdmin: PlasmaTradeAdmin,
  provider: Provider,
  initSqrtPrice: BN,
  tickSpacing: number,
  poolLiquidityParams: PoolLiquidityParam[]
) {
  const { tokenMintA, tokenMintB, poolAddress } = await initPool(
    plasmatradeAdmin,
    provider,
    initSqrtPrice,
    tickSpacing
  );

  const positionMints = [];

  for (let i = 0; i < poolLiquidityParams.length; i++) {
    const { tickLowerIndex, tickUpperIndex, isTokenMintA, tokenAmount } = poolLiquidityParams[i];

    const quote = await client.pool.getOpenPositionQuote({
      tickLowerIndex,
      tickUpperIndex,
      poolAddress,
      tokenMint: isTokenMintA ? tokenMintA : tokenMintB,
      tokenAmount,
      slippageTolerance: zeroSlippage,
      refresh: true,
    });

    const openTx = await client.pool.getOpenPositionTx({ provider, quote });

    const { tx, mint } = openTx;

    await tx.buildAndExecute();

    positionMints.push(mint);
  }

  return {
    poolAddress,
    tokenMintA,
    tokenMintB,
    positionMints,
  };
}
