import { getPoolMock, PlasmaTradeDALFileMock } from "../mocks/plasmatrade-dal";
import { Connection, PublicKey } from "@solana/web3.js";
import { PlasmaTradeDAL } from "../../src/dal/plasmatrade-dal";
import { PlasmaPoolData } from "@plasmatrade-so/plasmapool-client-sdk";

describe("Mocking PlasmaTrade DAL", () => {
  beforeEach(() => {
    PlasmaTradeDALFileMock.clearAllMocks();
  });

  it("test", async () => {
    const mockDal = new PlasmaTradeDAL(
      PublicKey.default,
      PublicKey.default,
      new Connection("http://google.com")
    );

    expect(await mockDal.getPool(PublicKey.default, true)).toEqual({});
    getPoolMock.mockImplementation(() => ({ test: 1 } as any as PlasmaPoolData));
    expect(await mockDal.getPool(PublicKey.default, true)).toEqual({ test: 1 });
  });
});
