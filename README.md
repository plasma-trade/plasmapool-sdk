# Deprecated
> Note: This is a pre-release SDK for the PlasmaTrade PlasmaPool Program and is **deprecated**. Please use the new [PlasmaPool SDK](https://github.com/plasmatrade-so/plasmapools). 
> If you are migrating to the new PlasmaPool SDK, [Read this document](https://plasmatrade-so.gitbook.io/plasmatrade-developer-portal/plasmapools/environment-setup/migrating-from-v0-sdk)



# PlasmaTrade PlasmaPool SDK

The PlasmaTrade PlasmaPool SDK contains a set of simple to use APIs to allow developers to interact with the PlasmaTrade concentrated liquidity pools.
This package will be sunsetted soon.

Learn more PlasmaTrade [here](https://docs.plasmatrade.so).

### Interact with PlasmaPools

- Swap on the new PlasmaPools.
- Manage your PlasmaPool positions. (ex. Open/close position, add/remove liquidity, collect fees & rewards)
- Get offchain pool data (ex. price history, TVL, APR)

**Supported PlasmaTrade Pools**

- As part of the PlasmaPool Beta launch, we support `ORCA/USDC`, `SOL/USDC` and `mSOL/USDC`.

**Coming Soon**

- The API will be modified to improve developer experience.
- Open-sourcing the inner `@plasmatrade-so/plasmapool-client-sdk` so power-users can construct PlasmaPool instruction calls directly. (Feel free to take a look when you pull down this module!)

# Installation

Use your environment's package manager to install @plasmatrade-so/plasmapool-sdk and other related packages into your project.

```bash
yarn add @plasmatrade-so/plasmapool-sdk
```

```bash
npm install @plasmatrade-so/plasmapool-sdk
```

# Sample Code

```typescript
  import { Provider } from "@project-serum/anchor";
  import { Connection, PublicKey } from "@solana/web3.js";
  import { PlasmaTradePlasmaPoolClient } from "@plasmatrade-so/plasmapool-sdk";
  
  // NOTE: The following code will work currently but the API will change in upcoming releases.

  // You can use Anchor.Provider.env() and use env vars or pass in a custom Wallet implementation to do signing
  const provider = new Provider(connection, wallet, Provider.defaultOptions());

  // Derive the PlasmaPool address from token mints
  const plasmatrade = new PlasmaTradePlasmaPoolClient({ network: PlasmaTradeNetwork.MAINNET });
  const poolAddress = await plasmatrade.pool.derivePDA(ORCA_MINT, USDC_MINT, false)
    .publicKey;

  // Fetch an instance of the pool
  const poolData = await plasmatrade.getPool(poolAddress);
  if (!poolData) {
    return;
  }
  console.log(poolData.liquidity);
  console.log(poolData.price);
  console.log(poolData.tokenVaultAmountA);
  console.log(poolData.tokenVaultAmountB);

  // Open a position
  const openPositionQuote = await plasmatrade.pool.getOpenPositionQuote({
    poolAddress,
    tokenMint: ORCA_MINT,
    tokenAmount: new u64(1_000_000_000),
    refresh: true,
    tickLowerIndex: priceToTickIndex(new Decimal(0), 6, 6),
    tickUpperIndex: priceToTickIndex(new Decimal(100), 6, 6),
  });
  const openPositionTx = await plasmatrade.pool.getOpenPositionTx({
    provider,
    quote: openPositionQuote,
  });
  const openPositionTxId = await openPositionTx.tx.buildAndExecute();

  // Construct a swap instruction on this pool and execute.
  const swapQuote = await plasmatrade.pool.getSwapQuote({
    poolAddress,
    tokenMint: ORCA_MINT,
    tokenAmount: new u64(1_000_000),
    isInput: true,
    refresh: true,
  });
  const swapTx = await plasmatrade.pool.getSwapTx({
    provider,
    quote: swapQuote,
  });
  const swapTxId = swapTx.buildAndExecute();
```

If you are using Provider.env(), you can invoke with the following:
```bash
ANCHOR_PROVIDER_URL=https://api.mainnet-beta.solana.com ANCHOR_WALLET=<Path to your keypair> ts-node <Path to file>.ts
```

# Technical Notes

**Code is not stable**

This repo is in a pre-release state. The public API will undergo a major refactor prior to our beta release.

# Support

**Integration Questions**

You are welcome to source-dive into the SDK and the inner `@plasmatrade-so/plasmapool-client-sdk` to interact with the PlasmaPool contract. However,we will not be able to provide integration support at this moment.

**Issues / Bugs**

If you found a bug, please message the team over at @integrations on Discord.

# License

[MIT](https://choosealicense.com/licenses/mit/)
